import nash.nash_core as nash_core
from nash.graphql_schema import graphql_schema as schema, DateTime
from nash import graphql_helper
from nash.graphql_helper import Query

from sgqlc.operation import Operation
from sgqlc.types import list_of


class GraphQlSignedRequestsMixin:
    """ Used as Mixin for NashApi """

    def list_account_stakes(self) -> schema.AccountStakes:
        """Get all stakes for an account

        Returns:
            nash.graphql_schema.AccountStakes: all stakes
        """
        _payload = nash_core.create_list_account_stakes_params()
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.list_account_stakes(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            stakes=True,
            total_dividends=True,
            total_staked_tokens=True
        )
        req.stakes.__fields__(
            return_rate=True,
            staked_tokens=True,
            start=True,
            status=True,
            stop=True,
            total_dividends=True
        )

        req.total_dividends.__fields__(
            amount=True,
            currency=True
        )
        req.total_staked_tokens.__fields__(
            amount=True,
            currency=True
        )
        res = self._exec_gql_query(op)
        return res.list_account_stakes

    def list_account_staking_dividends(self, month: int, year: int) -> schema.AccountStakingDividends:
        """Get staking dividends for account by month/year

        Args:
            month (int): month to search for
            year (int): year to search for

        Returns:
            nash.graphql_schema.AccountStakingDividends: the dividends
        """
        _payload = nash_core.create_list_account_staking_dividends_params(month, year)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.list_account_staking_dividends(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            dividends=True,
            paid_days_this_month=True,
            stakes_completed=True,
            stakes_in_progress=True,
            total_dividends=True
        )

        req.dividends.__fields__(
            asset=True,
            asset_name=True,
            dividend=True,
            id=True,
            paid_at=True,
            usd_dividend=True
        )

        req.total_dividends.__fields__(
            amount=True,
            currency=True
        )

        req.dividends.dividend.__fields__(
            amount=True,
            currency=True
        )
        req.dividends.usd_dividend.__fields__(
            amount=True,
            currency=True
        )

        res = self._exec_gql_query(op)
        return res.list_account_staking_dividends

    def list_account_staking_statements(self) -> list_of(schema.AccountStakingStatement):
        """ List all staking statements for account

        Returns:
            [nash.graphql_schema.AccountStakingStatement]: staking statements
        """
        _payload = nash_core.create_list_account_staking_statements_params()
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.list_account_staking_statements(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            active_stakes=True,
            id=True,
            month=True,
            total_dividends=True,
            year=True
        )
        req.total_dividends.__fields__(
            amount=True,
            currency=True
        )

        res = self._exec_gql_query(op)
        return res.list_account_staking_statements

    def get_account_balance(self, currency: str) -> schema.AccountBalance:
        """ Get the account balance for a specific currency (e.g. 'gas')

        Args:
            currency (str): e.g. 'gas'

        Returns:
            [nash.graphql_schema.AccountBalance]: balance

        Examples:
            >>> api.get_account_balance("eth")
            AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='30303030303030303030303030303030303030303030303030303030303030303030303030303030', name='Ethereum', symbol='eth', withdrawal_precision=8), available=CurrencyAmount(amount='21.5670000000000000000000000', currency='eth'), deposit_address=None, in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), personal=CurrencyAmount())
        """
        _currency = currency.lower()
        _payload = nash_core.create_get_account_balance_params(currency=_currency)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.get_account_balance(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            asset=True,  # = sgqlc.types.Field('Asset', graphql_name='asset')
            available=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='available')
            deposit_address=True,  # = sgqlc.types.Field(String, graphql_name='depositAddress')
            in_orders=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='inOrders')
            pending=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='pending')
            personal=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='personal')
        )
        graphql_helper.add_asset_fields(req.asset)
        req.available.__fields__(
            amount=True,
            currency=True
        )
        req.in_orders.__fields__(
            amount=True,
            currency=True
        )
        req.pending.__fields__(
            amount=True,
            currency=True
        )
        req.personal.__fields__(
            amount=True,
            currency=True
        )

        res = self._exec_gql_query(op)
        return res.get_account_balance

    def list_account_orders(self,
                            market_name: str = None,
                            status: list_of(schema.OrderStatus) = None,
                            before: str = None,
                            buy_or_sell: schema.OrderBuyOrSell = None,
                            limit: int = None,
                            range_start: str = None,
                            range_stop: str = None,
                            type: schema.OrderType = None) -> schema.OrderHistory:
        """ List the orders of this user.

        Args:
            market_name (str, optional): The market name to get account orders for
            status ([OrderStatus], optional): list of statuses to filter for
            before (str, optional): pagination cursor
            buy_or_sell (OrderBuyOrSell, optional): Whether to list only buy or sell orders
            limit (int, optional): Maximum amount of orders to return
            range_start (str, optional): iso8601 format
            range_stop (str, optional): iso8601 format
            type (OrderType), optional): list of order types to filter for

        Returns:
            [nash.graphql_schema.OrderHistory]: order history of this account

        Examples:
            >>> api.list_account_orders()
            OrderHistory(next=None, orders=[Order(amount=CurrencyAmount(amount='1.44000000', currency='eth'), buy_or_sell='SELL', cancel_at=None, cancellation_policy=None, id='5142749', limit_price=CurrencyPrice(), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING'), placed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), status='FILLED', stop_price=CurrencyPrice(), trades=[Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='vRrkuYrQ18Sr23rVWk_ul_40Br9zl_zoKXReeL6776g', limit_price=CurrencyPrice(amount='18.450', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='Cb0S8iQwuBJA2m_3CqajA0EvmkaLtlZi_u0GNVyiLZI', limit_price=CurrencyPrice(amount='18.452', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='6QOfv9PSjxtsgD3NQYb2RN38Ud23FWBznLk2_BqMFKo', limit_price=CurrencyPrice(amount='18.451', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='HV6nZQgigjJnUCV1JJRUiF7C7FcS0RegjHuS5Jf6fNc', limit_price=CurrencyPrice(amount='18.452', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING'))], type='MARKET', amount_remaining=CurrencyAmount(amount='0.00000000', currency='eth'))])

            >>> api.list_account_orders("gas_neo", ["OPEN"])
            >>> api.list_account_orders("gas_neo", buy_or_sell="BUY")
            >>> api.list_account_orders("gas_neo", buy_or_sell="BUY", range_start='2019-01-02T12:34:56Z', range_stop='2019-01-22T12:34:56Z')
        """
        _payload = nash_core.create_list_account_orders_params(market_name=market_name, status=status, before=before, buy_or_sell=buy_or_sell, limit=limit, range_start=range_start, range_stop=range_stop, type=type)

        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        # sgqlc needs the datetime range in DateTime format. If present then convert it from iso string to DateTime.
        if 'range_start' in payload:
            payload['range_start'] = DateTime(payload["range_start"])
        if 'range_stop' in payload:
            payload['range_stop'] = DateTime(payload["range_stop"])

        op = Operation(Query)
        req = op.list_account_orders(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.next()
        graphql_helper.add_order_fields(req.orders)

        res = self._exec_gql_query(op)
        return res.list_account_orders

    def list_account_transactions(self, cursor: str = None, fiat_symbol: str = None, limit: int = None) -> schema.ListAccountTransactionsResponse:
        """ List the transactions of this user account.

        Args:
            cursor (str, optional): offset to search for
            fiat_symbol (str, optional): fiat symbol for value representation of transaction
            limit (str, optional): how many results to return

        Returns:
            nash.graphql_schema.ListAccountTransactionsResponse: transactions of this user

        Example:
            >>> api.list_account_transactions()
        """
        _payload = nash_core.create_list_account_transactions_params(cursor=cursor, fiat_symbol=fiat_symbol, limit=limit)

        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)

        req = op.list_account_transactions(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.next_cursor()
        req.transactions().__fields__(
            address=True, block_datetime=True, block_index=True, blockchain=True, confirmations=True,
            fiat_value=True, status=True, txid=True, type=True, value=True
        )
        req.transactions().confirmations().__fields__(denominator=True, numerator=True)
        req.transactions().value().__fields__(amount=True, currency=True)

        # print(op)

        res = self._exec_gql_query(op)
        return res.list_account_transactions

    def get_account_order(self, id: int) -> schema.OrderHistory:
        """ List the orders of this user.

        Args:
            id (int): ID of order to retrieve

        Returns:
            nash.graphql_schema.OrderHistory: orders

        Examples:
            >>> orders = api.get_account_order(123)
        """
        _payload = nash_core.create_get_account_order_params(id=id)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        # list_order_params = ListOrdersParams.__to_graphql_input__({ 'timestamp': 123 })
        # print(payload)

        req = op.get_account_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        graphql_helper.add_order_fields(req)
        # print(op)

        res = self._exec_gql_query(op)
        return res.get_account_order

    def list_account_balances(self, ignore_low_balance: bool = False) -> list_of(schema.AccountBalance):
        """ List the balances of this user.

        Args:
            ignore_low_balance (bool, optional): currently unused

        Examples:
            >>> api.list_account_balances()
            [AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='0000000000000000000000000000000000000000', name='Ethereum', symbol='eth', withdrawal_precision=8), available=CurrencyAmount(amount='21.5670000000000000000000000', currency='eth'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), personal=CurrencyAmount(amount='0.029271040000000000', currency='eth')),
            AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='8462d1140c9dc51641c90a26a27849cca8d5e6e0', name='Basic Attention Token', symbol='bat', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='bat'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.00000000', currency='bat'), pending=CurrencyAmount(amount='0.00000000', currency='bat'), personal=CurrencyAmount(amount='11.000000000000000000', currency='bat')),
            AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='aed88173df2578fad078c48a33f0040364989fa8', name='USD Coin', symbol='usdc', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='usdc'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.00000000', currency='usdc'), pending=CurrencyAmount(amount='0.00000000', currency='usdc'), personal=CurrencyAmount(amount='51.000000000000000000', currency='usdc')),
            AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=8, hash='602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7', name='GAS', symbol='gas', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='gas'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.00000000', currency='gas'), pending=CurrencyAmount(amount='0.00000000', currency='gas'), personal=CurrencyAmount(amount='2.00000000', currency='gas')),
            AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=0, hash='c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b', name='NEO', symbol='neo', withdrawal_precision=0), available=CurrencyAmount(amount='7.3620487500000000000000000', currency='neo'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='neo'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='neo'), personal=CurrencyAmount(amount='5', currency='neo')),
            AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=8, hash='36dfd2c33d4948a8429d00fc00a5bd70e02520ef', name='nOS', symbol='nos', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='nos'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.00000000', currency='nos'), pending=CurrencyAmount(amount='0.00000000', currency='nos'), personal=CurrencyAmount(amount='50.00000000', currency='nos'))]

        Returns:
            [nash.graphql_schema.AccountBalance]: balances of this user
        """
        _payload = nash_core.create_list_account_balance_params(ignore_low_balance=ignore_low_balance)
        # print("_payload: %s " % _payload)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        # print(payload)
        op = Operation(Query)
        req = op.list_account_balances(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            asset=True,  # = sgqlc.types.Field('Asset', graphql_name='asset')
            available=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='available')
            deposit_address=True,  # = sgqlc.types.Field(String, graphql_name='depositAddress')
            in_orders=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='inOrders')
            pending=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='pending')
            personal=True  # = sgqlc.types.Field('CurrencyAmount', graphql_name='personal')
        )
        graphql_helper.add_asset_fields(req.asset)

        req.available.__fields__(
            amount=True,
            currency=True
        )  # CurrencyAmount
        req.in_orders.__fields__(
            amount=True,
            currency=True
        )  # CurrencyAmount
        req.pending.__fields__(
            amount=True,
            currency=True
        )  # CurrencyAmount
        req.personal.__fields__(
            amount=True,
            currency=True
        )  # CurrencyAmount

        # print(op)
        res = self._exec_gql_query(op)
        return res.list_account_balances

    def get_account_volumes(self) -> schema.AccountVolume:
        """Get volumes for an account

        Returns:
            nash.graphql_schema.AccountVolume: volume for this account
        """
        _payload = nash_core.create_list_account_volumes_params()
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.get_account_volumes(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            daily=True,
            monthly=True,
            yearly=True,
            maker_fee_rate=True,
            taker_fee_rate=True
        )
        req.daily()
        req.monthly()
        req.yearly()
        req.daily.account_limit()
        req.daily.account_limit.__fields__('currency', 'amount')
        req.daily.account_spend()
        req.daily.account_spend.__fields__('currency', 'amount')
        req.daily.account_volume()
        req.daily.account_volume.__fields__('currency', 'amount')
        req.daily.exchange_volume()
        req.daily.exchange_volume.__fields__('currency', 'amount')

        req.monthly.account_limit()
        req.monthly.account_limit.__fields__('currency', 'amount')
        req.monthly.account_spend()
        req.monthly.account_spend.__fields__('currency', 'amount')
        req.monthly.account_volume()
        req.monthly.account_volume.__fields__('currency', 'amount')
        req.monthly.exchange_volume()
        req.monthly.exchange_volume.__fields__('currency', 'amount')

        req.yearly.account_limit()
        req.yearly.account_limit.__fields__('currency', 'amount')
        req.yearly.account_spend()
        req.yearly.account_spend.__fields__('currency', 'amount')
        req.yearly.account_volume()
        req.yearly.account_volume.__fields__('currency', 'amount')
        req.yearly.exchange_volume()
        req.yearly.exchange_volume.__fields__('currency', 'amount')

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_account_volumes

    def list_movements(self, currency: str = None,
                       status: schema.MovementStatus = None,
                       type: schema.MovementType = None) -> list_of(schema.Movement):
        """List movements for an account

        Args:
            ignore_low_balance (bool, optional): currently unused
            currency (str, optional): currency to filter by
            status (MovementStatus, optional): MovementStatus to filter by
            type (MovementType, optional): MovementType to filter by

        Returns:
            [nash.graphql_schema.Movement]: movements of this account
        """
        _currency = currency.lower() if currency else None
        _payload = nash_core.create_list_movements_params(currency=_currency, status=status, type=type)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        # print(payload)
        op = Operation(Query)
        req = op.list_movements(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            address=True,  # = sgqlc.types.Field(String, graphql_name='address')
            confirmations=True,  # = sgqlc.types.Field(Int, graphql_name='confirmations')
            currency=True,  # = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')
            id=True,  # = sgqlc.types.Field(Int, graphql_name='id')
            quantity=True,  # = sgqlc.types.Field(CurrencyAmount, graphql_name='quantity')
            received_at=True,  # = sgqlc.types.Field(DateTime, graphql_name='receivedAt')
            status=True,  # = sgqlc.types.Field(MovementStatus, graphql_name='status')
            type=True,  # = sgqlc.types.Field(MovementType, graphql_name='type')
        )

        req.quantity.__fields__(
            amount=True,
            currency=True
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.list_movements

    def get_movement(self, id: int) -> schema.Movement:
        """Get a movement for account by id

        Args:
            id (int): Id of movement to retrieve

        Returns:
            nash.graphql_schema.Movement: a specific movement
        """
        _payload = nash_core.create_get_movement_params(id=id)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        # print(payload)
        op = Operation(Query)
        req = op.get_movement(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            address=True,  # = sgqlc.types.Field(String, graphql_name='address')
            confirmations=True,  # = sgqlc.types.Field(Int, graphql_name='confirmations')
            currency=True,  # = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')
            id=True,  # = sgqlc.types.Field(Int, graphql_name='id')
            quantity=True,  # = sgqlc.types.Field(CurrencyAmount, graphql_name='quantity')
            received_at=True,  # = sgqlc.types.Field(DateTime, graphql_name='receivedAt')
            status=True,  # = sgqlc.types.Field(MovementStatus, graphql_name='status')
            type=True,  # = sgqlc.types.Field(MovementType, graphql_name='type')
        )

        req.quantity.__fields__(
            amount=True,
            currency=True
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_movement

    def get_deposit_address(self, currency: str) -> schema.AccountDepositAddress:
        """Get a deposit address for a speciific currency

        Args:
            currency (str): currency to get deposit address for. e.g. 'eth'

        Returns:
            nash.graphql_schema.AccountDepositAddress: a blockchain deposit address

        Examples:
            >>> api.get_deposit_address("eth")
            AccountDepositAddress(address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', currency='eth')
        """
        _payload = nash_core.create_get_deposit_address_params(currency=currency)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        # print(payload)
        op = Operation(Query)
        req = op.get_deposit_address(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            address=True,
            currency=True
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_deposit_address

    def get_account_portfolio(self, fiat_symbol: str = None, period: schema.PortfolioGraphPeriod = "MONTH") -> schema.AccountPortfolio:
        """Gets account portfolio, optionally for a given fiat symbol or period

        Args:
            fiat_symbol (str, optional): Currency to view fiat balance in. Defaults to None.
            period (schema.PortfolioGraphPeriod, optional): Period to view portfolio by. Defaults to "MONTH".

        Returns:
            nash.graphql_schema.AccountPortfolio: portfolio
        """
        _payload = nash_core.create_get_account_portfolio_params(fiat_symbol=fiat_symbol, period=period)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        op = Operation(Query)
        req = op.get_account_portfolio(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            balances=True,
            total=True
        )

        req.balances.__fields__(
            allocation=True,
            asset=True,
            fiat_price=True,
            fiat_price_change=True,
            fiat_price_change_percent=True,
            total=True,
            total_fiat_price=True,
            total_fiat_price_change=True
        )

        req.balances.asset.__fields__(
            blockchain=True,
            blockchain_precision=True,
            hash=True,
            name=True,
            symbol=True,
        )
        req.total.__fields__(
            available_allocation=True,
            available_fiat_price=True,
            in_orders_allocation=True,
            in_orders_fiat_price=True,
            in_stakes_allocation=True,
            in_stakes_fiat_price=True,
            pending_allocation=True,
            pending_fiat_price=True,
            personal_allocation=True,
            personal_fiat_price=True,
            total_fiat_price=True,
            total_fiat_price_change=True,
            total_fiat_price_change_percent=True
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_account_portfolio

    def get_orders_for_movement(self, unit: str) -> schema.GetOrdersForMovementResponse:
        """ Get current asset nonce and and any orders that need to be resigned in order to add a new movement

        Args:
            unit (str): currency to query asset nonce and orders for

        Returns:
            nash.graphql_schema.GetOrdersForMovementResponse:

        Examples:
            >>> api.get_orders_for_movement('neo')
            GetOrdersForMovementResponse(asset_nonce=1, recycled_orders=[])
        """
        _payload = nash_core.create_get_orders_for_movement_params(unit=unit)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.get_orders_for_movement(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            asset_nonce=True,
            recycled_orders=True
        )
        req.recycled_orders.__fields__(
            blockchain=True,
            message=True
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_orders_for_movement

    def get_assets_nonces(self, assets: list) -> list_of(schema.AssetNonces):
        """ Get a list of active nonces for an asset specified in the list provided

        Args:
            assets (list(str)): A list of assets, e.g. ["neo", "eth"]

        Returns:
            [nash.graphql_schema.AssetNonces]:

        Examples:
            >>> api.get_assets_nonces(["neo", "eth"])
            [AssetNonces(asset='eth', nonces=[1]), AssetNonces(asset='neo', nonces=[0])]
        """
        _payload = nash_core.create_get_assets_nonces_params(assets)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Query)
        req = op.get_assets_nonces(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            asset=True,
            nonces=True
        )
        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        return res.get_assets_nonces
