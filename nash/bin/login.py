#!/usr/bin/env python3
"""
This cli utility will be installed as `nash-login`. To use it, just run `nash-login` in your shell.
You can also use it with staging:

    NASH_ENV=staging nash-login

To enable additional debug information:

    DEBUG=1 nash-login
"""
import argparse

from pprint import pformat
from nash import __version__
from nash import NashApi


# DEFAULT_EMAIL = "test@example.com"
DEFAULT_EMAIL = "chris+cas3@linuxuser.at"
DEFAULT_PASS = "hunter2"


def main():
    parser = argparse.ArgumentParser()

    # Show version
    parser.add_argument("--version", action="version",
                        version="Nash Python API Client v{version}".format(version=__version__))

    args = parser.parse_args()
    assert args  # else flake8 complains about unused variable
    # print(args)

    email = input("email: ") or DEFAULT_EMAIL
    password = input("password: ") or DEFAULT_PASS

    api = NashApi()
    api.login(email, password)

    # After successful login, print infos
    print("Login successful.")
    print("- Chain indices:", pformat(api.chain_indices))
    print("- Account:", pformat(api.account))

    print("---")
    print("Init params (for use in test-gql.py):")
    print(api.nash_core_init_params)


if __name__ == "__main__":
    main()
