from nash.graphql_schema import graphql_schema as schema
from nash.graphql_helper import Mutation
from nash import nash_core
from nash.helpers import normalize_amount_for_market_precision, normalize_price_for_market_precision, \
    verify_and_normalize_market_name, verify_and_normalize_buy_or_sell, verify_and_normalize_cancellation_policy
from nash.logger import logger
from nash.exceptions import GraphQlError
from nash.nash_core import CurrencyAmount

from sgqlc.operation import Operation
from sgqlc.types import list_of


class GraphQlMutatingRequestsMixin:
    """ Used as Mixin for NashApi """

    def place_market_order(self,
                           market_name: str,
                           amount: CurrencyAmount,
                           buy_or_sell: schema.OrderBuyOrSell,
                           retry_on_missing_nonces: bool = True) -> schema.OrderPlaced:
        """ Place a market order.

        Args:
            market_name (str): Market to place order for (e.g. 'gas_neo')
            amount (CurrencyAmount): Amount to buy or sell as a CurrencyAmount (i.e. CurrencyAmount.FromString("0.12345 eth"))
            buy_or_sell (str): Either "BUY" or "SELL"

        Returns:
            nash.graphql_schema.OrderPlaced: placed order

        Raises:
            AttributeError: if invalid market_name or buy_or_sell or amount argument

        Examples:
            Sell 1.44 ETH (for NEO):

            >>> api.place_market_order("eth_neo", CurrencyAmount("1.44","eth"), "SELL")
            OrderPlaced(id='5142749', status='PENDING')
        """
        original_args = locals()

        market = self.markets[market_name]
        from_unit = market.a_unit
        to_unit = market.b_unit

        if buy_or_sell.upper() == "BUY":
            from_unit = market.b_unit
            to_unit = market.a_unit

        # Validate and normalize inputs
        buy_or_sell = verify_and_normalize_buy_or_sell(buy_or_sell)
        amount = normalize_amount_for_market_precision(amount, market)
        # Build payload and sign
        _payload = nash_core.create_place_market_order_params(
            amount=amount.ToParams(),
            buy_or_sell=buy_or_sell,
            market_name=market_name,
            nonces_from=self.asset_nonces[from_unit],
            nonces_to=self.asset_nonces[to_unit]
        )

        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        # Build GraphQL request
        op = Operation(Mutation)
        req = op.place_market_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.id()
        req.status()

        # Execute GraphQL request
        # we catch invalid asset nonces error and retry again if we get that error
        try:
            res = self._exec_gql_query(op)
            return res.place_market_order
        except GraphQlError as error:
            message = error.args[0][0]
            if 'missing_asset_nonces' in message['message']:
                logger.info("Stale asset nonces, will retry")
                self.get_all_asset_nonces()
                if retry_on_missing_nonces:
                    del original_args['self']
                    original_args['retry_on_missing_nonces'] = False
                    return self.place_market_order(**original_args)
            raise error

    def place_stop_market_order(self,
                                market_name: str,
                                amount: CurrencyAmount,
                                buy_or_sell: schema.OrderBuyOrSell,
                                stop_price: str,
                                retry_on_missing_nonces: bool = True) -> schema.OrderPlaced:
        """ Place a stop market order.

        Args:
            market_name (str): Market to place order for (e.g. 'gas_neo')
            amount (CurrencyAmount): Amount to buy or sell as a CurrencyAmount (i.e. CurrencyAmount.FromString("0.12345 eth"))
            buy_or_sell (str): Either "BUY" or "SELL"
            stop_price (str): Price to stop at, always specified in `b_unit per a_unit` of market (in eth_neo, stop price is X neo per eth, where we supply X)

        Returns:
            nash.graphql_schema.OrderPlaced:

        Raises:
            AttributeError: if invalid market_name, buy_or_sell, amount or stop_price argument

        Examples:
            Sell 1.44 GAS (for NEO) with stop price of 0.1:

            >>> api.place_stop_market_order("gas_neo", CurrencyAmount.FromString("1.44 gas)", "SELL", "0.1")
            OrderPlaced(id='3150', status='PENDING')
        """

        original_args = locals()

        market = self.markets[market_name]

        # determine which to use for asset nonces
        from_unit = market.a_unit
        to_unit = market.b_unit
        if buy_or_sell.upper() == "BUY":
            from_unit = market.b_unit
            to_unit = market.a_unit

        # Validate and normalize inputs
        market_name = verify_and_normalize_market_name(market_name, self.markets)
        buy_or_sell = verify_and_normalize_buy_or_sell(buy_or_sell)
        amount = normalize_amount_for_market_precision(amount, market)
        stop_price = normalize_price_for_market_precision(stop_price, market)

        # Build payload and sign
        _payload = nash_core.create_place_stop_market_order_params(
            amount=amount.ToParams(),
            buy_or_sell=buy_or_sell,
            market_name=market_name,
            stop_price=nash_core.create_currency_price_params(stop_price, market.b_unit, market.a_unit),
            nonces_from=self.asset_nonces[from_unit],
            nonces_to=self.asset_nonces[to_unit]
        )
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        # Build GraphQL request
        op = Operation(Mutation)
        req = op.place_stop_market_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.id()
        req.status()

        # Execute GraphQL request
        try:
            res = self._exec_gql_query(op)
            return res.place_stop_market_order
        except GraphQlError as error:
            message = error.args[0][0]
            if 'missing_asset_nonces' in message['message']:
                logger.info("Stale asset nonces, will retry")
                self.get_all_asset_nonces()
                if retry_on_missing_nonces:
                    del original_args['self']
                    original_args['retry_on_missing_nonces'] = False
                    return self.place_stop_market_order(**original_args)
            raise error

    def place_limit_order(self,
                          market_name: str,
                          amount: CurrencyAmount,
                          buy_or_sell: schema.OrderBuyOrSell,
                          cancellation_policy: schema.OrderCancellationPolicy,
                          limit_price: str,
                          allow_taker: bool,
                          cancel_at: int = None,
                          retry_on_missing_nonces: bool = True) -> schema.OrderPlaced:
        """ Place a limit order.

        Args:
            market_name (str): Market to place order for (e.g. 'gas_neo')
            amount (CurrencyAmount): Amount to buy or sell as a CurrencyAmount (i.e. CurrencyAmount.FromString("0.12345 eth"))
            buy_or_sell (str): Either "BUY" or "SELL"
            cancellation_policy (str/schema.OrderCancellationPolicy): cancellation policy
            limit_price (str): Limit price, always specified in `b_unit per a_unit` of market (in eth_neo, stop price is X neo per eth, where we supply X)
            allow_taker (bool): Whether to allow a taker order
            cancel_at (int, optional): timestamp when to cancel for the `GOOD_TIL_TIME` cancellation policy

        Returns:
            nash.graphql_schema.OrderPlaced:

        Raises:
            AttributeError: if invalid market_name, buy_or_sell, amount or stop_price argument

        Examples:
            Sell 1.44 GAS (for NEO) with limit price of 0.1:

            >>> api.place_limit_order("gas_neo", "1.44", "SELL", "GOOD_TIL_CANCELLED", "0.1", allow_taker=True)
            OrderPlaced(id='3150', status='PENDING')
        """
        original_args = locals()

        market = self.markets[market_name]

        # determine which to use for asset nonces
        from_unit = market.a_unit
        to_unit = market.b_unit
        if buy_or_sell.upper() == "BUY":
            from_unit = market.b_unit
            to_unit = market.a_unit

        # Validate and normalize inputs
        market_name = verify_and_normalize_market_name(market_name, self.markets)
        buy_or_sell = verify_and_normalize_buy_or_sell(buy_or_sell)
        cancellation_policy = verify_and_normalize_cancellation_policy(cancellation_policy)  # todo
        amount = normalize_amount_for_market_precision(amount, market)
        limit_price = normalize_price_for_market_precision(limit_price, market)

        assert isinstance(allow_taker, bool), "allow_taker must be a bool"
        assert cancel_at is None or isinstance(cancel_at, int), "cancel_at must be None or an int"

        # Build payload and sign
        _payload = nash_core.create_place_limit_order_params(
            allow_taker=allow_taker,
            amount=amount.ToParams(),
            buy_or_sell=buy_or_sell,
            cancellation_policy=cancellation_policy,
            limit_price=nash_core.create_currency_price_params(limit_price, market.b_unit, market.a_unit),
            market_name=market_name,
            cancel_at=cancel_at,
            nonces_from=self.asset_nonces[from_unit],
            nonces_to=self.asset_nonces[to_unit]
        )
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        # Build GraphQL request
        op = Operation(Mutation)
        req = op.place_limit_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.id()
        req.status()

        # Execute GraphQL request
        try:
            res = self._exec_gql_query(op)
            return res.place_limit_order
        except GraphQlError as error:
            message = error.args[0][0]
            if 'missing_asset_nonces' in message['message']:
                logger.info("Stale asset nonces, will retry")
                self.get_all_asset_nonces()
                if retry_on_missing_nonces:
                    del original_args['self']
                    original_args['retry_on_missing_nonces'] = False
                    return self.place_limit_order(**original_args)
            raise error

    def place_stop_limit_order(self,
                               market_name: str,
                               amount: CurrencyAmount,
                               buy_or_sell: schema.OrderBuyOrSell,
                               cancellation_policy: schema.OrderCancellationPolicy,
                               limit_price: str,
                               stop_price: str,
                               allow_taker: bool,
                               cancel_at: int = None,
                               retry_on_missing_nonces: bool = True) -> schema.OrderPlaced:
        """ Place a stop-limit order.

        Args:
            market_name (str): Market to place order for (e.g. 'gas_neo')
            amount (CurrencyAmount): Amount to buy or sell as a CurrencyAmount (i.e. CurrencyAmount.FromString("0.12345 eth"))
            buy_or_sell (str): Either "BUY" or "SELL"
            cancellation_policy (str/schema.OrderCancellationPolicy): cancellation policy
            limit_price (str): Limit price, always specified in `b_unit per a_unit` of market (in eth_neo, stop price is X neo per eth, where we supply X)
            stop_price (str): Price to stop at, always specified in `b_unit per a_unit` of market (in eth_neo, stop price is X neo per eth, where we supply X)
            allow_taker (bool): Whether to allow a taker order
            cancel_at (int, optional): timestamp when to cancel, for the `GOOD_TIL_TIME` cancellation policy

        Returns:
            nash.graphql_schema.OrderPlaced:

        Raises:
            AttributeError: if invalid market_name, buy_or_sell, amount or stop_price argument

        Examples:
            Sell 1.44 GAS (for NEO) with limit price of 0.1 and stop price of 0.2:

            >>> api.place_stop_limit_order("gas_neo", "1.44", "SELL", "GOOD_TIL_CANCELLED", limit_price="0.1", stop_price="0.2", allow_taker=True)
            OrderPlaced(id='3150', status='PENDING')
        """
        original_args = locals()

        market = self.markets[market_name]

        # determine which to use for asset nonces
        from_unit = market.a_unit
        to_unit = market.b_unit
        if buy_or_sell.upper() == "BUY":
            from_unit = market.b_unit
            to_unit = market.a_unit

        # Validate and normalize inputs
        market_name = verify_and_normalize_market_name(market_name, self.markets)
        buy_or_sell = verify_and_normalize_buy_or_sell(buy_or_sell)
        cancellation_policy = verify_and_normalize_cancellation_policy(cancellation_policy)  # todo
        amount = normalize_amount_for_market_precision(amount, market)
        limit_price = normalize_price_for_market_precision(limit_price, market)
        stop_price = normalize_price_for_market_precision(stop_price, market)

        assert isinstance(allow_taker, bool), "allow_taker must be a bool"
        assert cancel_at is None or isinstance(cancel_at, int), "cancel_at must be None or an int"

        # Build payload and sign
        _payload = nash_core.create_place_stop_limit_order_params(
            allow_taker=allow_taker,
            amount=amount.ToParams(),
            buy_or_sell=buy_or_sell,
            cancellation_policy=cancellation_policy,
            limit_price=nash_core.create_currency_price_params(limit_price, market.b_unit, market.a_unit),
            stop_price=nash_core.create_currency_price_params(stop_price, market.b_unit, market.a_unit),
            market_name=market_name,
            cancel_at=cancel_at,
            nonces_from=self.asset_nonces[from_unit],
            nonces_to=self.asset_nonces[to_unit]
        )
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        # Build GraphQL request
        op = Operation(Mutation)
        req = op.place_stop_limit_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.id()
        req.status()

        # Execute GraphQL request
        try:
            res = self._exec_gql_query(op)
            return res.place_stop_limit_order
        except GraphQlError as error:
            message = error.args[0][0]
            if 'missing_asset_nonces' in message['message']:
                logger.info("Stale asset nonces, will retry")
                self.get_all_asset_nonces()
                if retry_on_missing_nonces:
                    del original_args['self']
                    original_args['retry_on_missing_nonces'] = False
                    return self.place_stop_limit_order(**original_args)
            raise error

    def cancel_all_orders(self, market_name: str = None) -> schema.CanceledOrders:
        """Cancels all orders.  If `market_name` is specified, only cancels all orders in that market.

        Args:
            market_name (str, optional): Market to cancel orders in. Defaults to None.

        Returns:
            nash.graphql_schema.CanceledOrders:
        """
        _payload = nash_core.create_cancel_all_orders_params(market_name=market_name)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Mutation)

        req = op.cancel_all_orders(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            accepted=True
        )

        # print(op)
        res = self._exec_gql_query(op)
        return res.cancel_all_orders

    def cancel_order(self, order_id: str, market_name: str) -> schema.CanceledOrder:
        """Cancels an order.

        Args:
            order_id (str): Order ID to cancel
            market_name (str): Market of order to cancel

        Returns:
            nash.graphql_schema.CanceledOrder: the cancelled order

        Examples:
            >>> api.cancel_order("5142749", "eth_neo")
            CanceledOrder(order_id='5142749')
        """
        _payload = nash_core.create_cancel_order_params(order_id=order_id, market_name=market_name)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Mutation)
        req = op.cancel_order(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            order_id=True
        )
        # print(op)
        res = self._exec_gql_query(op)
        return res.cancel_order

    def get_states(self) -> schema.GetStatesResponse:
        """
        Gets a list of current balance states from the matching engine

        Returns:
            nash.graphql_schema.GetStatesResponse
        """

        _payload = nash_core.create_get_states_params()
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Mutation)
        req = op.get_states(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            states=True,
            recycled_orders=True
        )

        req.recycled_orders.__fields__(
            blockchain=True,
            message=True
        )

        req.states.__fields__(
            address=True,
            balance=True,
            blockchain=True,
            message=True,
            nonce=True
        )
        req.states.balance.__fields__(
            amount=True,
            currency=True
        )

        res = self._exec_gql_query(op)
        return res.get_states

    def sign_states(self, state_list: list, recycled_orders: list = []) -> schema.SignStatesResponse:
        """
        Submits a list of signed balance states to the matching engine
        and gathers the same states with the matching engine signatures

        Args:
            state_list (list): A list of {'message':'abc','blockchain':'neo'} items

        Returns:
            nash.graphql_schema.SignStatesResponse:  The signed messages from the matching engine
        """
        signature, payload = nash_core.sign_state_list(self.nash_core_config, state_list, recycled_orders)
        op = Operation(Mutation)
        req = op.sign_states(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})

        req.__fields__(
            server_signed_states=True
        )
        req.server_signed_states.__fields__(
            message=True,
            blockchain=True
        )
        res = self._exec_gql_query(op)
        return res.sign_states

    def sync_states(self, server_signed_states: list_of(schema.ServerSignedState), override_movements=False) -> schema.SyncStatesResponse:
        """Tells the system to sync a set of balances to the blockchain

        Args:
            server_signed_states ([ServerSignedState]): ServerSignedState list retrieved from `sign_states` call.
            override_movements (bool, optional): Whether to clear pending deposits. Defaults to False.

        Returns:
            nash.graphql_schema.SyncStatesResponse
        """
        sync_list = [{'message': state.message,
                      'blockchain': state.blockchain}
                     for state in server_signed_states]

        _payload = nash_core.create_sync_states_params(sync_list, override_movements)
        signature, payload = nash_core.sign_payload(self.nash_core_config, _payload)

        op = Operation(Mutation)
        req = op.sync_states(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            result=True
        )
        res = self._exec_gql_query(op)
        return res.sync_states

    def get_sign_and_sync_states(self) -> schema.SyncStatesResponse:
        """
        Combines get_states, sign_states, and sync states into 1 easy to use method.
        This should be called by a client for every 50 to 100 orders they place in order
        to reconcile their balances with the blockchain.

        Returns:
            nash.graphql_schema.SignStatesResponse: The signed messages from the matching engine
        """
        get_states = self.get_states()
        signable_list = [{'message': state.message,
                          'blockchain': state.blockchain}
                         for state in get_states.states]

        recycled = [{'message': state.message,
                     'blockchain': state.blockchain}
                    for state in get_states.recycled_orders]
        signed_states = self.sign_states(signable_list, recycled)

        sync_states = self.sync_states(signed_states.server_signed_states)

        # if we sync ok, then we'll need to update the asset nonces
        if sync_states.result == 'ok':
            self.get_all_asset_nonces()

        return sync_states

    def sign_deposit_request(self, currency: str, amount: str, address: str = None) -> (schema.Movement, dict):
        # Intentionally undocumented because user should not do this from an API client.
        _currency = currency.lower()

        movement_data = self.get_orders_for_movement(_currency)
        amount = nash_core.create_currency_amount_params(amount, _currency)

        # if address is not specified, we will look it up for the user
        if not address:
            address = self.address_for_currency(_currency)

        _payload = nash_core.create_add_movement_params(address, amount, "DEPOSIT", movement_data.asset_nonce, movement_data.recycled_orders)
        signature, payload, blockchain_data = nash_core.sign_payload(self.nash_core_config, _payload)

        op = Operation(Mutation)
        req = op.add_movement(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            address=True,
            currency=True,
            id=True,
            nonce=True,
            public_key=True,
            quantity=True,
            received_at=True,
            signature=True,
            status=True,
            type=True
        )

        res = self._exec_gql_query(op)
        # for clients that may want the blockchain data for invoking purposes, we'll set it as an attribute
        setattr(res.add_movement, 'blockchain_data', blockchain_data)
        return res.add_movement, blockchain_data

    def sign_withdraw_request(self, currency: str, amount: str, address: str = None) -> (schema.Movement, dict):
        # Intentionally undocumented because user should not do this from an API client.

        _currency = currency.lower()

        movement_data = self.get_orders_for_movement(_currency)

        amount = nash_core.create_currency_amount_params(amount, _currency)

        # if address is not specified, we will look it up for the user
        if not address:
            address = self.address_for_currency(_currency)

        _payload = nash_core.create_add_movement_params(address, amount, "WITHDRAWAL", movement_data.asset_nonce, movement_data.recycled_orders)
        signature, payload, blockchain_data = nash_core.sign_payload(self.nash_core_config, _payload)
        op = Operation(Mutation)
        req = op.add_movement(payload=payload, signature={"public_key": self.signing_pubkey, "signed_digest": signature})
        req.__fields__(
            address=True,
            currency=True,
            id=True,
            nonce=True,
            public_key=True,
            quantity=True,
            received_at=True,
            signature=True,
            status=True,
            type=True
        )

        # print(op)
        res = self._exec_gql_query(op)
        setattr(res.add_movement, 'blockchain_data', blockchain_data)
        return res.add_movement, blockchain_data
