# -*- coding: utf-8 -*-

"""Top-level package for neo-python-core."""

__author__ = """Nash"""
__version__ = '1.2.2-dev'

from nash.api import NashApi
from nash.nash_core import CurrencyAmount, CurrencyPrice

assert all([NashApi, CurrencyAmount, CurrencyPrice])
