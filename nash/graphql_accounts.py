from nash.graphql_helper import Mutation
from nash.graphql_schema import graphql_schema as schema

from sgqlc.operation import Operation


class GraphQlAccountsRequestsMixin:
    """ Used as Mixin for NashApi """

    def do_signup(self,
                  email: str,
                  passwd: str,
                  country_code: str,
                  full_name: str,
                  state_code: str = "",
                  recaptcha='',
                  referral_code='') -> schema.SuccessMessage:
        op = Operation(Mutation)

        identity = {'country_code': country_code, 'full_name': full_name, 'state_code': state_code}
        acct = {'email': email, 'password': passwd, 'identity': identity}

        req = op.sign_up(account=acct, recaptcha=recaptcha, referral_code=referral_code)
        req.slug()
        req.message()

        res = self._exec_gql_query(op)
        return res.sign_up

    def do_verify_account(self, email: str):
        op = Operation(Mutation)
        req = op.verification_skip(email=email)  # NOQA

        res = self._exec_gql_query(op)
        return res.verification_skip

    def do_two_factor_auth(self, two_factor_code: str):
        op = Operation(Mutation)
        req = op.two_factor_login(two_fa=two_factor_code)
        req.account()
        req.account.__fields__(
            creating_account=True,
            email=True,
            encrypted_secret_key=True,
            encrypted_secret_key_tag=True,
            encrypted_secret_key_nonce=True,
            id=True,
            identity=False,
            login_error_count=True,
            two_factor=True,
            two_factor_error_count=True,
            verified=True,
            wallets=True
        )

        req.account.wallets.__fields__(
            address=True,
            blockchain=True,
            chain_index=True,
            public_key=True
        )
        req.server_encryption_key()

        res = self._exec_gql_query(op)

        return res.two_factor_login

    def do_add_wallets_and_keys(self, core_params: dict, encrypted_key_data: dict, api_cookie: str):

        op = Operation(Mutation)

        wallets = [
            {
                "blockchain": "ETH",
                "address": core_params["Wallets"]["eth"]["Address"].upper(),
                "public_key": core_params["Wallets"]["eth"]["PublicKey"],
                "chain_index": 1
            }, {
                "blockchain": "NEO",
                "address": core_params["Wallets"]["neo"]["Address"],
                "public_key": core_params["Wallets"]["neo"]["PublicKey"],
                "chain_index": 1
            },
            # {
            #     "blockchain": "btc",
            #     "address": core_params["Wallets"]["btc"]["Address"],
            #     "public_key": core_params["Wallets"]["btc"]["PublicKey"]
            # },
        ]

        req = op.add_keys_with_wallets(  # NOQA
            encrypted_secret_key=encrypted_key_data["encryptedKey"],
            encrypted_secret_key_nonce=encrypted_key_data["nonce"],
            encrypted_secret_key_tag=encrypted_key_data["tag"],
            signature_public_key=core_params["PayloadSigning"]["PublicKey"],
            wallets=wallets
        )

        res = self._exec_gql_query(op)
        return res.add_keys_with_wallets
