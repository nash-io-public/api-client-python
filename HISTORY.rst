=======
History
=======

1.2.2-dev (in progress)
-----------------------
* ...


1.2.1 (2019-12-16)
------------------
* Fix bug in API error handling, which was introduced with checking for maintenance mode (`MR 13 <https://gitlab.com/nash-io-public/api-client-python/merge_requests/13>`_)


1.2.0 (2019-12-13)
------------------
* Added ARM support (eg. for Raspberry Pi)
* Removed broken Windows support. Now throws an exception.


1.1.0 (2019-12-13)
------------------
* Fixes to time range selection in ``list_account_orders``
* ``MaintenanceModeError`` - will be thrown on API requests when the backend is in maintenance mode
* API uses sandbox environment by default. Needs specific environment setting ``production`` to use prod.


1.0.6 (2019-12-04)
------------------
* Link support


1.0.0 (2019-04-18)
------------------
* Initial release
