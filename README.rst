Getting started
---------------

**THIS SDK will be discontinued in new future, please refer to api-client-typescript**

**Requires Python 3** (``brew install python3``)

Setup a virtual env and install the dependencies (needed when cloning or downloading a dist)::

    # Setup and activate a venv with Python 3
    python3 -m venv venv
    . venv/bin/activate

    # Install from Pypi
    (venv) pip install nash-api


To get the current version of the Python client::

    nash-utils --version

You can test connectivity with `nash-utils` like this::

    nash-utils --list-markets

Using the sandbox environment: 

    NASH_ENV=sandbox nash-utils --list-markets

You can also use the sandbox environment by passing the `environment` argument to the NashApi constructor:

    api = NashApi(environment='sandbox')

See the help with ``nash-utils -h``.

Note: Any script interacting with Nash should be aware that the backend can be in maintenance mode anytime.
In case of maintenance any API request, the Python API will throw a ``nash.exceptions.MaintenanceModeError``

------------

Below is a basic example of usage:


.. code-block:: python

    >>> from nash import NashApi, CurrencyAmount, CurrencyPrice
    >>> api = NashApi()  # sandbox is the default environment
    
    >>> # Some calls don't require login
    >>> api.list_markets()
    [Market(id='market:eth_neo', a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', min_trade_increment='0.00001', min_trade_increment_b='0.001', name='eth_neo', status='RUNNING'),
    Market(id='market:eth_bat', a_unit='eth', a_unit_precision=7, b_unit='bat', b_unit_precision=7, min_tick_size='0.01', min_trade_size='0.0200000', min_trade_size_b='10.0000000', min_trade_increment='0.00001', min_trade_increment_b='0.01', name='eth_bat', status='RUNNING'),
    ...]

    >>> # Now let's login
    >>> api.login("you@email.com", "password")

    >>> # Getting the current balances. Note: balances in the trading account appear under "available"
    >>> api.list_account_balances(ignore_low_balance=True)
    [AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='0000000000000000000000000000000000000000', name='Ethereum', symbol='eth', withdrawal_precision=8), available=CurrencyAmount(amount='21.5670000000000000000000000', currency='eth'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), personal=CurrencyAmount(amount='0.029271040000000000', currency='eth')),
    AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='8462d1140c9dc51641c90a26a27849cca8d5e6e0', name='Basic Attention Token', symbol='bat', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='bat'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.00000000', currency='bat'), pending=CurrencyAmount(amount='0.00000000', currency='bat'), personal=CurrencyAmount(amount='11.000000000000000000', currency='bat')),
    AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='aed88173df2578fad078c48a33f0040364989fa8', name='USD Coin', symbol='usdc', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='usdc'), deposit_address='433CEAA0B6E98AB129DBDC4A3435F652ADA7BE7D', in_orders=CurrencyAmount(amount='0.00000000', currency='usdc'), pending=CurrencyAmount(amount='0.00000000', currency='usdc'), personal=CurrencyAmount(amount='51.000000000000000000', currency='usdc')),
    AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=8, hash='602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7', name='GAS', symbol='gas', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='gas'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.00000000', currency='gas'), pending=CurrencyAmount(amount='0.00000000', currency='gas'), personal=CurrencyAmount(amount='2.00000000', currency='gas')),
    AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=0, hash='c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b', name='NEO', symbol='neo', withdrawal_precision=0), available=CurrencyAmount(amount='7.3620487500000000000000000', currency='neo'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='neo'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='neo'), personal=CurrencyAmount(amount='5', currency='neo')),
    AccountBalance(asset=Asset(blockchain='neo', blockchain_precision=8, deposit_precision=8, hash='36dfd2c33d4948a8429d00fc00a5bd70e02520ef', name='nOS', symbol='nos', withdrawal_precision=8), available=CurrencyAmount(amount='0.00000000', currency='nos'), deposit_address='ALU8PyEw5csvUKdccez65jM6WGAo6m7sEX', in_orders=CurrencyAmount(amount='0.00000000', currency='nos'), pending=CurrencyAmount(amount='0.00000000', currency='nos'), personal=CurrencyAmount(amount='50.00000000', currency='nos'))]
 
    >>> api.get_account_balance("eth")
    AccountBalance(asset=Asset(blockchain='eth', blockchain_precision=18, deposit_precision=8, hash='30303030303030303030303030303030303030303030303030303030303030303030303030303030', name='Ethereum', symbol='eth', withdrawal_precision=8), available=CurrencyAmount(amount='21.5670000000000000000000000', currency='eth'), deposit_address=None, in_orders=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), pending=CurrencyAmount(amount='0.0000000000000000000000000', currency='eth'), personal=CurrencyAmount())

    >>> # Placing an order. Note: ensure that you have funds in your trading account.
    >>> # Needs to show under "available" in the account balance response.
    >>> api.place_market_order("eth_neo", CurrencyAmount("1.44","eth"), "SELL")
    OrderPlaced(id='5142749', status='PENDING')

    >>> # List all orders of this user
    >>> api.list_account_orders()
    OrderHistory(next=None, orders=[Order(amount=CurrencyAmount(amount='1.44000000', currency='eth'), buy_or_sell='SELL', cancel_at=None, cancellation_policy=None, id='5142749', limit_price=CurrencyPrice(), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING'), placed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), status='FILLED', stop_price=CurrencyPrice(), trades=[Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='vRrkuYrQ18Sr23rVWk_ul_40Br9zl_zoKXReeL6776g', limit_price=CurrencyPrice(amount='18.450', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='Cb0S8iQwuBJA2m_3CqajA0EvmkaLtlZi_u0GNVyiLZI', limit_price=CurrencyPrice(amount='18.452', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='6QOfv9PSjxtsgD3NQYb2RN38Ud23FWBznLk2_BqMFKo', limit_price=CurrencyPrice(amount='18.451', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING')), Trade(amount=CurrencyAmount(amount='0.10000000', currency='eth'), executed_at=datetime.datetime(2019, 9, 24, 20, 55, 13, 389520, tzinfo=datetime.timezone.utc), id='HV6nZQgigjJnUCV1JJRUiF7C7FcS0RegjHuS5Jf6fNc', limit_price=CurrencyPrice(amount='18.452', currency_a='neo', currency_b='eth'), market=Market(a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', name='eth_neo', status='RUNNING'))], type='MARKET', amount_remaining=CurrencyAmount(amount='0.00000000', currency='eth'))])

    >>> # List all orders of this user and a specific market and status
    >>> api.list_account_orders("gas_neo", ["OPEN"])

    >>> # Cancelling an order (Note: it may take some time to actually cancel the order after this call returns)
    >>> api.cancel_order("5142749", "eth_neo")
    CanceledOrder(order_id='5142749')

Handle maintenance mode:

    >>> from nash import NashApi
    >>> from nash.exceptions import MaintenanceModeError
    >>>
    >>> api = NashApi(environment='sandbox')
    >>>
    >>> try:
    >>>     api.list_markets()
    >>> except MaintenanceModeError:
    >>>     # Wait and retry or fail, depending on preference and purpose

You can use the sandbox environment by setting the `NASH_ENV` environment variable to `sandbox`:

    NASH_ENV=sandbox python
    NASH_ENV=sandbox nash-utils --list-markets


Debug Options:
--------------

Environment variables:

* ``NASH_ENV=sandbox`` - Does the requests against the sandbox environment instead of local
* ``LOGLEVEL=debug`` - Set loglevel to [``debug``, ``info`` (default), ``warning``, ``error``, ``critical``]
* ``DEBUG=1`` - enables DEBUG loglevel and prints all GQL requests and responses
* ``GQL_PRINT_REQUEST=1`` - prints the GQL request before sending
* ``GQL_PRINT_RESPONSE=1`` - prints the raw GQL json response after receiving


Documentation
-------------

Generate the documentation with ``make docs`` or ``make docs-watch``.

The Python inline docs are using `Google Style Python Docstrings <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_ (`2 <http://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings>`_)

We use the Sphinx autodoc extension to automatically generate the docs:

* http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html


Feedback & Issues
-----------------

Please submit feedback, issues and improvement requests via https://gitlab.com/nash-io-public/api-client-python/issues
