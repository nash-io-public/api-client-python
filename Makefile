.PHONY: clean clean-test clean-pyc clean-docs clean-build lint docs help docs-watch
.DEFAULT_GOAL := help
define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"
CURDIR := $(shell pwd)

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test clean-docs ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -f .coverage
	rm -fr htmlcov/
	rm -rf .pytest_cache

clean-docs: ## remove doc build artifacts
	$(MAKE) -C docs clean

lint: ## check style with flake8 and pycodestyle
	pycodestyle nash
	flake8 nash

docs: ## generate Sphinx HTML documentation, including API docs
	# rm -f docs/nash.rst
	# rm -f docs/modules.rst
	# sphinx-apidoc -o docs/ nash
	# $(MAKE) -C docs clean
	# $(MAKE) -C docs html
	# $(BROWSER) docs/_build/html/index.html
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

docs-watch: ## Watch docs and rebuild on changes. Includes live-reload.
	$(MAKE) -C docs watch
